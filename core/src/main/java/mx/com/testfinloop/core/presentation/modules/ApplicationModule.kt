package mx.com.testfinloop.core.presentation.modules

import android.accounts.AccountManager
import android.app.Application
import android.content.Context
import android.content.res.Resources
import dagger.Module
import dagger.Provides
import mx.com.testfinloop.core.domain.executors.JobExecutor
import mx.com.testfinloop.core.domain.executors.PostExecutionThread
import mx.com.testfinloop.core.domain.executors.ThreadExecutor
import mx.com.testfinloop.core.domain.executors.UIThread
import mx.com.testfinloop.core.presentation.authentication.services.KeyStoreManager
import mx.com.testfinloop.core.presentation.managers.AccountsManager
import mx.com.testfinloop.core.presentation.managers.SharedPreferencesManager
import mx.com.testfinloop.core.presentation.managers.UserDataManager
import org.jetbrains.anko.accountManager
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: Application) {
    @Provides
    @Singleton
    fun providesApplicationContext(): Application = this.application

    @Provides
    @Singleton
    fun providesContext(): Context = application.applicationContext

    @Provides
    @Singleton
    fun providesResources(): Resources = application.resources

    @Provides
    @Singleton
    fun provideSharedPreferences(application: Application): SharedPreferencesManager =
        SharedPreferencesManager(application.applicationContext)

    @Provides
    @Singleton
    fun provideKeyStoreManager(
        application: Application,
        preferencesManager: SharedPreferencesManager
    ): KeyStoreManager =
        KeyStoreManager(application.applicationContext, preferencesManager)

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor = jobExecutor

    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread = uiThread

    @Provides
    @Singleton
    fun providesAccountManager(context: Context): AccountManager = context.accountManager

    @Provides
    @Singleton
    fun providesUserDataManager(
        preferences: SharedPreferencesManager,
        keyStoreManager: KeyStoreManager
    ): UserDataManager = UserDataManager(preferences, keyStoreManager)

    @Provides
    @Singleton
    fun providesAccountsManager(
        resources: Resources,
        accountManager: AccountManager,
        userDataManager: UserDataManager
    ): AccountsManager = AccountsManager(resources, accountManager, userDataManager)
}