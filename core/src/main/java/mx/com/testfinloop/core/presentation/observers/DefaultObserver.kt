package mx.com.testfinloop.core.presentation.observers

import androidx.lifecycle.MutableLiveData
import io.reactivex.observers.DisposableObserver

abstract class DefaultObserver<Result, State>(
    val observer: MutableLiveData<State>? = null
) : DisposableObserver<Result>()