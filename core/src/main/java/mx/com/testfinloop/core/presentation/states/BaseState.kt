package mx.com.testfinloop.core.presentation.states

sealed class BaseState<out T> {
    data class Data<T>(val data: T) : BaseState<T>()
    data class LocalizedError<T>(val error: Int) : BaseState<T>()
    data class Exception<T>(val error: String) : BaseState<T>()
    data class LoadingVisibility<T>(val visibility: Int) : BaseState<T>()
}