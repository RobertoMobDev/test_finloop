package mx.com.testfinloop.core.presentation.views

import androidx.lifecycle.MutableLiveData

data class BaseLiveData<Result>(
    val customObserver: MutableLiveData<Result>,
    val loadingObserver: MutableLiveData<Int>,
    val showErrorObserver: MutableLiveData<Int>,
    val showExceptionObserver: MutableLiveData<String>,
    val refreshTokenExceptionObserver: MutableLiveData<String>
)