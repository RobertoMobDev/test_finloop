package mx.com.testfinloop.core.presentation.managers

import android.annotation.SuppressLint
import java.util.*

@SuppressLint("ConstantLocale")
object FormatManager {

    private val locale: Locale by lazy {
        Locale.getDefault()
    }

    const val DATE_FORMAT = "dd MMM yyyy"
    private const val PERIOD_FORMAT = "dd 'de' MMMM 'de' yyyy"
    private const val MONTH_FORMAT = "MMM"
    private const val COMPLETE_MONTH_FORMAT = "MMMM"
    private const val DAY_FORMAT = "dd"
    private const val PERIOD_FORMAT_NAME_DAY = "EEEE',' dd MMM yyyy"
    private const val DAY_NAME_MONTH_AND_YEAR_FORMAT = "EEEE',' dd 'de' MMMM 'de' yyyy"
    private const val DAY_NAME_MONTH_AND_YEAR_FORMAT_NO_COMMA = "EEEE dd 'de' MMMM yyyy"
    private const val FLIGHT_STATUS_DATE_FORMAT = "yyyy-MM-dd"
    private const val YEAR_MONTH_DAY_FORMAT = "yyyy-MM-dd"
    private const val DAY_MONTH_YEAR_FORMAT = "dd-MM-yyyy"
    private const val DAY_NAME = "EEEE"
    private const val DATE_SLASH_TIME_FORMAT = "dd MMM yyyy / HH:mm 'h'"

}