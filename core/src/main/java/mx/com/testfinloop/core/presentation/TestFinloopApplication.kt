package mx.com.testfinloop.core.presentation

import android.app.Application
import android.content.Context
import mx.com.testfinloop.core.R
import mx.com.testfinloop.core.presentation.components.ApplicationComponent
import mx.com.testfinloop.core.presentation.components.DaggerApplicationComponent
import mx.com.testfinloop.core.presentation.modules.ApplicationModule
import mx.com.testfinloop.core.presentation.modules.NetModule

class TestFinloopApplication : Application() {

    val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .netModule(
                NetModule(
                    getString(R.string.base_url)
                )
            )
            .applicationModule(ApplicationModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
    }

}

/**
 * @return The application component from any class that have access or extends the Context class
 */
fun Context.getApplicationComponent(): ApplicationComponent =
    (this.applicationContext as TestFinloopApplication).applicationComponent