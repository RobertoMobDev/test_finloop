package mx.com.testfinloop.core.domain.entities

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable

data class AuthToken(val token: String) : Parcelable {

    val data: Bundle
        get() = Bundle().apply {
            putString(AUTH_TOKEN, this@AuthToken.token)
        }

    constructor(parcel: Parcel) : this(parcel.readString() ?: "")

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(token)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AuthToken> {

        const val AUTH_TOKEN: String = "AUTH_TOKEN"

        override fun createFromParcel(parcel: Parcel): AuthToken {
            return AuthToken(parcel)
        }

        override fun newArray(size: Int): Array<AuthToken?> {
            return arrayOfNulls(size)
        }
    }
}