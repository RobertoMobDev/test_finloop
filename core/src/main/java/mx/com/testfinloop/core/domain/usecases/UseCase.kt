package mx.com.testfinloop.core.domain.usecases

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import mx.com.testfinloop.core.domain.executors.PostExecutionThread
import mx.com.testfinloop.core.domain.executors.ThreadExecutor

abstract class UseCase<Result, Params>(
    private val threadExecutor: ThreadExecutor,
    private val postExecutionThread: PostExecutionThread
) : Interactor<Result, Params> {

    abstract fun createObservable(params: Params): Observable<Result>

    private val disposables: CompositeDisposable by lazy { CompositeDisposable() }

    override fun execute(observer: DisposableObserver<Result>, params: Params) {
        val observable: Observable<Result> = this.createObservable(params)
            .subscribeOn(Schedulers.from(this.threadExecutor))
            .observeOn(this.postExecutionThread.getScheduler())
        this.disposables.add(observable.subscribeWith(observer))
    }

    fun dispose() {
        if (!this.disposables.isDisposed) {
            this.disposables.dispose()
        }
    }

}