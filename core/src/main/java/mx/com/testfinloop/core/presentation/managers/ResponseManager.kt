package mx.com.testfinloop.core.presentation.managers

import com.google.gson.Gson
import mx.com.testfinloop.core.domain.entities.BaseErrorResponse
import mx.com.testfinloop.core.domain.exceptions.TestException
import retrofit2.Response
import java.io.IOException

object ResponseManager {

    fun processErrorResponse(response: Response<*>): Throwable {
        val gson = Gson()
        val base: BaseErrorResponse
        return try {
            base = gson.fromJson<BaseErrorResponse>(response.errorBody()?.string(), BaseErrorResponse::class.java)
            TestException(base.error)
        } catch (exception: IOException) {
            TestException("Error in response")
        }

    }
}