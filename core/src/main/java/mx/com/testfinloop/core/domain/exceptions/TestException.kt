package mx.com.testfinloop.core.domain.exceptions

open class TestException(message: String) : Exception(message)