package mx.com.testfinloop.core.presentation.utils

import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.AnimRes
import androidx.annotation.AnimatorRes
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.jakewharton.rxbinding.view.RxView
import mx.com.testfinloop.core.data.android.lifecycle.SmartLiveDataAndroid
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

fun String.encrypt(): String =
    CryptoUtils.getInstance().encryptOrNull(this) ?: ""

fun String.decrypt(): String =
    CryptoUtils.getInstance().decryptOrNull(this) ?: ""

fun <T> MutableLiveData<T>.observeWithValidations(
    viewLifeCycleOwner: LifecycleOwner,
    output: (response: T?) -> Unit
) {
    if (!this.hasObservers()) {
        this.observe(viewLifeCycleOwner, Observer {
            output(it)
        })
    }
}

fun <T> SmartLiveDataAndroid<T>.observeWithValidations(
    viewLifeCycleOwner: LifecycleOwner,
    output: (response: T) -> Unit
) {
    if (!this.hasObservers()) {
        this.observe(viewLifeCycleOwner, Observer {
            it?.let { output(it) }
        })
    }
}

fun View.clickEvent(): LiveData<Void> {
    val listener = MutableLiveData<Void>()
    RxView.clicks(this)
        .throttleFirst(1000, TimeUnit.MILLISECONDS)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe {
            listener.value = it
        }
    return listener
}

fun View.clickEvent(duration: Long = 500L, output: () -> Unit) {
    RxView.clicks(this)
        .throttleFirst(duration, TimeUnit.MILLISECONDS)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe { output() }
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun ViewGroup.inflate(layoutRes: Int): View =
    LayoutInflater.from(context).inflate(layoutRes, this, false)

fun ImageView.loadRemoteUrl(
    url: Int,
    options: RequestOptions,
    loading: ((visibility: Int) -> Unit)? = null
) {
    try {
        loading?.let { it(View.VISIBLE) }
        val glideBuilder = Glide.with(this.context)
            .load(url)
            .apply(options)
            .transition(DrawableTransitionOptions.withCrossFade())
        loading?.let {
            glideBuilder.listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    it(View.GONE)
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    it(View.GONE)
                    return false
                }
            })
        }
        glideBuilder.into(this)
    } catch (exception: Exception) {
        Log.e("exception", exception.message ?: "")
    }
}

fun ImageView.loadUrl(url: Int, requestOptions: RequestOptions) {
    Glide.with(context)
        .load(url)
        .apply(requestOptions)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(this)
}

fun View.show(show: Boolean) {
    if (show) {
        this.visible()
    } else {
        this.gone()
    }
}

fun FragmentManager.performReplacingTransaction(@IdRes containerViewId: Int, fragment: Fragment,
                                                @AnimRes @AnimatorRes enterAnim: Int? = null, @AnimRes @AnimatorRes exitAnim: Int? = null,
                                                @AnimRes @AnimatorRes popEnterAnim: Int? = null, @AnimRes @AnimatorRes popExitAnim: Int? = null,
                                                backStackTag: String? = null, allowStateLoss: Boolean? = null) {
    val transaction = this.beginTransaction()
    if (enterAnim != null && exitAnim != null) {
        if (popEnterAnim != null && popExitAnim != null) {
            transaction.setCustomAnimations(enterAnim, exitAnim, popEnterAnim, popExitAnim)
        } else {
            transaction.setCustomAnimations(enterAnim, exitAnim)
        }
    }
    transaction.replace(containerViewId, fragment)
    if (backStackTag != null) {
        transaction.addToBackStack(backStackTag)
    }
    if (allowStateLoss != null && allowStateLoss) {
        transaction.commitAllowingStateLoss()
    } else {
        transaction.commit()
    }
}