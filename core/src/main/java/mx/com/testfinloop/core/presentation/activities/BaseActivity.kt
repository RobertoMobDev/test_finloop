package mx.com.testfinloop.core.presentation.activities

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import mx.com.testfinloop.core.R
import mx.com.testfinloop.core.presentation.getApplicationComponent
import mx.com.testfinloop.core.presentation.utils.performReplacingTransaction

abstract class BaseActivity : AppCompatActivity() {

    val testAccountManager by lazy { getApplicationComponent().accountsManager() }
    val userDataManager by lazy { this.getApplicationComponent().userDataManager() }

    private val transitions by lazy {
        Transitions(
            R.anim.left_in,
            R.anim.left_out,
            R.anim.rigth_in,
            R.anim.rigth_out
        )
    }

    abstract fun getLayoutResId(): Int

    abstract fun initView(savedInstanceState: Bundle?)

    override fun onCreate(savedInstanceState: Bundle?) {
        this.overridePendingTransition(transitions.onCreateEnterAnimation, transitions.onCreateExitAnimation)
        super.onCreate(savedInstanceState)
        if (getLayoutResId() != 0) { setContentView(getLayoutResId()) }
        initView(savedInstanceState)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.overridePendingTransition(transitions.onBackPressedEnterAnimation, transitions.onBackPressedExitAnimation)
    }

    open fun replaceFragment(fragment: Fragment, @IdRes container: Int, addToBackStack: Boolean) {
        supportFragmentManager.performReplacingTransaction(
            container,
            fragment,
            this.transitions.onCreateEnterAnimation,
            this.transitions.onCreateExitAnimation,
            this.transitions.onBackPressedEnterAnimation,
            this.transitions.onBackPressedExitAnimation,
            if (addToBackStack) fragment::class.java.name else null,
            false
        )
    }

    open fun replaceFragmentNoAnimation(
        fragment: Fragment, @IdRes container: Int,
        addToBackStack: Boolean
    ) {
        supportFragmentManager.performReplacingTransaction(
            container,
            fragment,
            backStackTag = if (addToBackStack) fragment::class.java.name else null,
            allowStateLoss = false
        )
    }

    class Transitions(
        val onCreateEnterAnimation: Int,
        val onCreateExitAnimation: Int,
        val onBackPressedEnterAnimation: Int,
        val onBackPressedExitAnimation: Int
    )

}