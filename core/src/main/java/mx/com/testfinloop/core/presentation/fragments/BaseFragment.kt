package mx.com.testfinloop.core.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import mx.com.testfinloop.core.R
import mx.com.testfinloop.core.presentation.activities.BaseActivity
import mx.com.testfinloop.core.presentation.getApplicationComponent
import mx.com.testfinloop.core.presentation.utils.performReplacingTransaction

abstract class BaseFragment : Fragment() {

    val userDataManager by lazy { requireActivity().getApplicationComponent().userDataManager() }

    private val transitions by lazy {
        BaseActivity.Transitions(
            R.anim.left_in,
            R.anim.left_out,
            R.anim.rigth_in,
            R.anim.rigth_out
        )
    }

    abstract fun getLayout(): Int

    abstract fun initView(view: View, savedInstanceState: Bundle?)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(getLayout(), container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view, savedInstanceState)
    }

    open fun replaceFragment(fragment: Fragment, @IdRes container: Int, addToBackStack: Boolean) {
        fragmentManager?.performReplacingTransaction(
            container,
            fragment,
            this.transitions.onCreateEnterAnimation,
            this.transitions.onCreateExitAnimation,
            this.transitions.onBackPressedEnterAnimation,
            this.transitions.onBackPressedExitAnimation,
            if (addToBackStack) fragment::class.java.name else null,
            false
        )
    }

    open fun replaceFragmentNoAnimation(
        fragment: Fragment, @IdRes container: Int,
        addToBackStack: Boolean
    ) {
        childFragmentManager.performReplacingTransaction(
            container,
            fragment,
            backStackTag = if (addToBackStack) fragment::class.java.name else null,
            allowStateLoss = false
        )
    }

    open fun replaceParentFragment(
        fragment: Fragment, @IdRes container: Int,
        addToBackStack: Boolean
    ) {
        parentFragment?.fragmentManager?.performReplacingTransaction(
            container,
            fragment,
            this.transitions.onCreateEnterAnimation,
            this.transitions.onCreateExitAnimation,
            this.transitions.onBackPressedEnterAnimation,
            this.transitions.onBackPressedExitAnimation,
            if (addToBackStack) fragment::class.java.name else null,
            false
        )
    }

}