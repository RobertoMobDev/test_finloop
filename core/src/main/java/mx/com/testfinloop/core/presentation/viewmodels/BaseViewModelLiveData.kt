package mx.com.testfinloop.core.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import mx.com.testfinloop.core.presentation.views.BaseLiveData

abstract class BaseViewModelLiveData<Result> : ViewModel() {

    fun getCustomLiveData(): BaseLiveData<Result> =
        BaseLiveData(
            MutableLiveData(),
            MutableLiveData(),
            MutableLiveData(),
            MutableLiveData(),
            MutableLiveData()
        )

    fun <Q> buildLiveData(observer: MutableLiveData<Q>): BaseLiveData<Q> =
        BaseLiveData(
            observer,
            MutableLiveData(),
            MutableLiveData(),
            MutableLiveData(),
            MutableLiveData()
        )

}