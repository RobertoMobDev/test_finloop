package mx.com.testfinloop.core.domain.usecases

import io.reactivex.observers.DisposableObserver

interface Interactor<Result, Params> {
    fun execute(observer: DisposableObserver<Result>, params: Params)
}