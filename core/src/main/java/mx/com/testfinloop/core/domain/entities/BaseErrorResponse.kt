package mx.com.testfinloop.core.domain.entities

import com.google.gson.annotations.SerializedName

open class BaseErrorResponse(
    @field:SerializedName("error")
    private var _error: String? = null
) {
    val error: String
        get() = this._error ?: ""
}