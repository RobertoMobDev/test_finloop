package mx.com.testfinloop.core.presentation.components

import android.accounts.AccountManager
import android.app.Application
import android.content.res.Resources
import dagger.Component
import mx.com.testfinloop.core.domain.executors.PostExecutionThread
import mx.com.testfinloop.core.domain.executors.ThreadExecutor
import mx.com.testfinloop.core.presentation.authentication.services.KeyStoreManager
import mx.com.testfinloop.core.presentation.managers.AccountsManager
import mx.com.testfinloop.core.presentation.managers.SharedPreferencesManager
import mx.com.testfinloop.core.presentation.managers.UserDataManager
import mx.com.testfinloop.core.presentation.modules.ApplicationModule
import mx.com.testfinloop.core.presentation.modules.NetModule
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, NetModule::class])
interface ApplicationComponent {

    fun applicationContext(): Application

    fun sharedPreferences(): SharedPreferencesManager

    fun resources(): Resources

    fun keyStoreManager(): KeyStoreManager

    fun threadExecutor(): ThreadExecutor

    fun postExecutionThread(): PostExecutionThread

    @Named("main_retrofit")
    fun mainRetrofit(): Retrofit

    fun accountManager(): AccountManager

    fun userDataManager(): UserDataManager

    fun accountsManager(): AccountsManager

    fun okHttpClient(): OkHttpClient

}