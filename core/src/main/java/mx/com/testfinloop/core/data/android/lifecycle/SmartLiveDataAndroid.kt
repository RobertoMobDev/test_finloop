package mx.com.testfinloop.core.data.android.lifecycle

class SmartLiveDataAndroid<T> : AndroidBaseLiveData<T>() {

    var value: T?
        set(value) = value?.let { setNextValue(it) }
            ?: throw NullPointerException("You cannot set null value for SmartLiveData")
        get() = nextValue

    public override fun postValue(value: T) {
        super.postValue(value)
    }

    public override fun setNextValue(value: T) {
        super.setNextValue(value)
    }

}