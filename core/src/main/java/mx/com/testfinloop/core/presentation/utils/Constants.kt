package mx.com.testfinloop.core.presentation.utils

object Constants {

    //Preferences
    const val IS_FIRST_TIME = "is.first.time"
    const val USER_EMAIL = "user.email"
    const val USER_PASSWORD = "user.password"
    const val AUTH_TOKEN = "auth.token"

}