package mx.com.testfinloop.core.presentation.modules

import android.app.Application
import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import dagger.Module
import dagger.Provides
import mx.com.testfinloop.core.presentation.managers.FormatManager
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetModule(private val baseUrl: String) {

    companion object {
        const val TIMEOUT_TIME = 300000L
        const val CONNECT_TIMEOUT = "CONNECT_TIMEOUT"
        const val READ_TIMEOUT = "READ_TIMEOUT"
        const val WRITE_TIMEOUT = "WRITE_TIMEOUT"
    }

    @Provides
    @Singleton
    fun provideOkHttpCache(context: Application): Cache {
        val cacheSize = 10L * 1024L * 1024L // 10 MiB
        return Cache(context.cacheDir, cacheSize)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
            .readTimeout(TIMEOUT_TIME, TimeUnit.MILLISECONDS)
            .connectTimeout(TIMEOUT_TIME, TimeUnit.MILLISECONDS)
            .addInterceptor(buildTimeoutInterceptor())

            httpClient.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))

        return httpClient.build()
    }

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder()
        .setDateFormat(FormatManager.DATE_FORMAT)
        .setExclusionStrategies(object : ExclusionStrategy {
            override fun shouldSkipField(f: FieldAttributes): Boolean {
                return f.annotations.none { it is SerializedName }
            }

            override fun shouldSkipClass(clazz: Class<*>): Boolean {
                return false
            }
        })
        .create()

    @Provides
    @Named("main_retrofit")
    @Singleton
    fun provideMainRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(this.baseUrl)
        .client(okHttpClient)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    private fun buildTimeoutInterceptor() = Interceptor { chain ->
        val request = chain.request()
        var connectTimeout = chain.connectTimeoutMillis()
        var readTimeout = chain.readTimeoutMillis()
        var writeTimeout = chain.writeTimeoutMillis()

        val connectNew = request.header(CONNECT_TIMEOUT) ?: ""
        val readNew = request.header(READ_TIMEOUT) ?: ""
        val writeNew = request.header(WRITE_TIMEOUT) ?: ""

        val builder = request.newBuilder()

        if (connectNew.isNotEmpty()) {
            connectTimeout = connectNew.toInt()
            builder.removeHeader(CONNECT_TIMEOUT)
        }
        if (readNew.isNotEmpty()) {
            readTimeout = readNew.toInt()
            builder.removeHeader(READ_TIMEOUT)
        }
        if (writeNew.isNotEmpty()) {
            writeTimeout = writeNew.toInt()
            builder.removeHeader(WRITE_TIMEOUT)
        }

        chain.withConnectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
            .withReadTimeout(readTimeout, TimeUnit.MILLISECONDS)
            .withWriteTimeout(writeTimeout, TimeUnit.MILLISECONDS)
            .proceed(builder.build())
    }

}