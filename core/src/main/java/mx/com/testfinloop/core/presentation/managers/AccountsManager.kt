package mx.com.testfinloop.core.presentation.managers

import android.accounts.Account
import android.accounts.AccountManager
import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import mx.com.testfinloop.core.R
import mx.com.testfinloop.core.domain.entities.AuthToken
import java.lang.ref.WeakReference
import javax.inject.Inject

class AccountsManager @Inject constructor(
    private val mResources: Resources,
    private val accountManager: AccountManager,
    private val userDataManager: UserDataManager
) {

    private val email: String
        get() = this.userDataManager.getUserEmail()

    private val packageName: String
        get() = this.mResources.getString(R.string.app_package_name)

    fun getAllAccounts(): Array<Account> =
        this.accountManager.accounts ?: emptyArray()

    fun getFdaAccounts(): Array<Account> =
        this.accountManager.getAccountsByType(packageName)

    fun getAccountByEmail(email: String): Account? =
        this.getFdaAccounts().firstOrNull { it.name == email }

    fun getAuthToken(context: Context): String =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.getAccountByEmail(this.email)?.let {
                AccountManager.get(context).getUserData(it, AuthToken.AUTH_TOKEN) ?: ""
            } ?: ""
        } else {
            with(this.userDataManager.getAuthToken()) {
                if (this.isNotEmpty()) this else getAccountByEmail(email)?.name ?: ""
            }
        }

    fun accountExists(): Boolean =
        this.getFdaAccounts().any { it.type == this.packageName }

    fun addAccount(activity: WeakReference<Activity>?) {
        activity?.get()?.let {
            this.accountManager.addAccount(this.packageName, null, null, null, it, null, null)
        }
    }

    fun addAccountExplicitly(account: Account, password: String, bundle: Bundle): Boolean =
        this.accountManager.addAccountExplicitly(account, password, bundle)

    fun removeAccountFromManager(email: String, activity: WeakReference<Activity>?) {
        activity?.get()?.let { act -> getAccountByEmail(email)?.let { removeAccount(it, act) } }
    }

    fun removeAllAccounts(activity: WeakReference<Activity>?) {
        activity?.get()
            ?.let { act -> this.getFdaAccounts().forEach { removeAccount(it, act) } }
    }

    fun setUserData(newToken: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.getAccountByEmail(this.email)?.let {
                this.accountManager.setUserData(it, AuthToken.AUTH_TOKEN, newToken)
            }
        } else {
            this.userDataManager.saveAuthToken(newToken)
        }
    }

    private fun removeAccount(account: Account, activity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            accountManager.removeAccount(account, activity, null, null)
        } else {
            accountManager.removeAccount(account, null, null)
        }
    }
}
