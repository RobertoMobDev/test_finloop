package mx.com.testfinloop.core.presentation.managers

import mx.com.testfinloop.core.presentation.authentication.services.KeyStoreManager
import mx.com.testfinloop.core.presentation.utils.Constants

class UserDataManager(
    private val preferencesManager: SharedPreferencesManager,
    private val keyStoreManager: KeyStoreManager
) {

    fun saveIsFirstTime(boolean: Boolean) {
        this.preferencesManager.setSharedPreference(Constants.IS_FIRST_TIME, boolean)
    }

    fun isFirstTime(defaultValue: Boolean): Boolean =
        this.preferencesManager.getSharedPreference(Constants.IS_FIRST_TIME, defaultValue)

    fun saveUserEmail(userName: String) {
        this.keyStoreManager.saveDataWithSafeMode(Constants.USER_EMAIL, userName)
    }

    fun getUserEmail(): String =
        this.keyStoreManager.retrieveDataWithSafeMode(Constants.USER_EMAIL)

    fun saveUserPassword(password: String) {
        this.keyStoreManager.saveSensitiveData(Constants.USER_PASSWORD, password)
    }

    fun getUserPassword(): String =
        this.keyStoreManager.retrieveSensitiveData(Constants.USER_PASSWORD)

    fun saveAuthToken(token: String) {
        this.keyStoreManager.saveDataWithSafeMode(Constants.AUTH_TOKEN, token)
    }

    fun getAuthToken(): String =
        this.keyStoreManager.retrieveDataWithSafeMode(Constants.AUTH_TOKEN)

}