package mx.com.testfinloop.presentation.account

import android.Manifest
import android.accounts.AbstractAccountAuthenticator
import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings.Global.getString
import android.widget.Toast
import androidx.core.app.ActivityCompat
import mx.com.testfinloop.core.R
import mx.com.testfinloop.login.presentation.activities.LoginActivity

class Authenticator(private val context: Context?)  : AbstractAccountAuthenticator(context) {

    private val handler = Handler()

    override fun getAuthTokenLabel(p0: String?): String? = null

    override fun confirmCredentials(
        p0: AccountAuthenticatorResponse?,
        p1: Account?,
        p2: Bundle?
    ): Bundle? = null

    override fun updateCredentials(
        p0: AccountAuthenticatorResponse?,
        p1: Account?,
        p2: String?,
        p3: Bundle?
    ): Bundle? = null

    override fun getAuthToken(
        p0: AccountAuthenticatorResponse?,
        p1: Account?,
        p2: String?,
        p3: Bundle?
    ): Bundle? = null

    override fun hasFeatures(
        p0: AccountAuthenticatorResponse?,
        p1: Account?,
        p2: Array<out String>?
    ): Bundle? = null

    override fun editProperties(p0: AccountAuthenticatorResponse?, p1: String?): Bundle? = null

    override fun addAccount(
        response: AccountAuthenticatorResponse?,
        accountType: String?,
        authTokenType: String?,
        requiredFeatures: Array<out String>?,
        options: Bundle?
    ): Bundle {

        val accountManager: AccountManager = AccountManager.get(context)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1 || ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {
            if (accountManager.getAccountsByType(accountType).isNotEmpty()) {
                val message = "Only one account allowed"
                val bundle = Bundle()
                bundle.putInt(
                    AccountManager.KEY_ERROR_CODE,
                    AccountManager.ERROR_CODE_UNSUPPORTED_OPERATION
                )
                bundle.putString(AccountManager.KEY_ERROR_MESSAGE, message)
                handler.post { Toast.makeText(context, message, Toast.LENGTH_LONG).show() }
                return bundle
            }
            val intent = Intent(requireContext(), LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
            intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, accountType)
            val bundle = Bundle()
            bundle.putParcelable(AccountManager.KEY_INTENT, intent)
            return bundle
        }

        val message = "permissions are required"
        val bundle = Bundle()
        bundle.putInt(
            AccountManager.KEY_ERROR_CODE,
            AccountManager.ERROR_CODE_UNSUPPORTED_OPERATION
        )
        bundle.putString(AccountManager.KEY_ERROR_MESSAGE, message)
        handler.post { Toast.makeText(context, message, Toast.LENGTH_LONG).show() }
        return bundle
    }

    private fun requireContext(): Context {
        if (this.context == null) throw NullPointerException("Context is null.")
        return this.context
    }

}