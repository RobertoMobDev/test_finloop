package mx.com.testfinloop.login.domain.usecases

import io.reactivex.Observable
import mx.com.testfinloop.core.domain.executors.PostExecutionThread
import mx.com.testfinloop.core.domain.executors.ThreadExecutor
import mx.com.testfinloop.core.domain.usecases.UseCase
import mx.com.testfinloop.login.domain.entities.request.LoginRequest
import mx.com.testfinloop.login.domain.entities.response.LoginResponse
import mx.com.testfinloop.login.domain.repositoryabstractions.LoginRepository
import javax.inject.Inject

class LoginUseCase @Inject constructor(
    private val validateLoginUseCase: ValidateLoginUseCase,
    private val saveUserUseCase: SaveUserUseCase,
    private val loginRepository: LoginRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<LoginResponse, LoginRequest>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: LoginRequest): Observable<LoginResponse> =
        Observable.just(params).flatMap { this.validateLoginUseCase.createObservable(it) }
            .flatMap { this.loginRepository.login(params) }
            .flatMap { this.saveUserUseCase.saveData(params, it) }
            .map { it }
}