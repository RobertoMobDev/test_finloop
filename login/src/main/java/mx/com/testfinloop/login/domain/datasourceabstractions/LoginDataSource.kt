package mx.com.testfinloop.login.domain.datasourceabstractions

import io.reactivex.Observable
import mx.com.testfinloop.login.domain.entities.request.LoginRequest
import mx.com.testfinloop.login.domain.entities.response.LoginResponse

interface LoginDataSource {
    fun login(loginRequest: LoginRequest): Observable<LoginResponse>
}