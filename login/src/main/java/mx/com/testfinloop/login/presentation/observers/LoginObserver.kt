package mx.com.testfinloop.login.presentation.observers

import android.view.View
import mx.com.testfinloop.core.data.android.lifecycle.SmartLiveDataAndroid
import mx.com.testfinloop.core.domain.exceptions.TestException
import mx.com.testfinloop.core.presentation.observers.DefaultObserver
import mx.com.testfinloop.login.R
import mx.com.testfinloop.login.domain.LoginException
import mx.com.testfinloop.login.domain.entities.response.LoginResponse
import mx.com.testfinloop.login.presentation.states.LoginState
import java.io.IOException

class LoginObserver(private val loginLiveData: SmartLiveDataAndroid<LoginState>) :
    DefaultObserver<LoginResponse, LoginState>() {


    override fun onStart() {
        this.loginLiveData.setNextValue(LoginState.LoadingVisibility(View.VISIBLE))
        super.onStart()
    }

    override fun onComplete() {
        this.loginLiveData.setNextValue(LoginState.LoadingVisibility(View.GONE))
    }

    override fun onNext(response: LoginResponse) {
        this.loginLiveData.setNextValue(LoginState.Login(response))
    }

    override fun onError(error: Throwable) {
        this.onComplete()
        when (error) {
            is IOException -> notifyError(R.string.no_internet_connection)
            is LoginException -> useCase(error)
            is TestException -> notifyException(error.localizedMessage ?: "")
            else -> notifyError(R.string.overall_generic_error)
        }
    }

    private fun useCase(error: LoginException) {
        when (error.validationType) {
            LoginException.Type.USERNAME_EMPTY_ERROR ->
                this.loginLiveData.setNextValue(LoginState.UserError(R.string.login_empty_user_error))
            LoginException.Type.USERNAME_WRONG_FORMAT_ERROR ->
                this.loginLiveData.setNextValue(LoginState.UserError(R.string.login_wrong_format_user_error))
            LoginException.Type.PASSWORD_EMPTY_ERROR ->
                this.loginLiveData.setNextValue(LoginState.PasswordError(R.string.login_empty_password_error))
            LoginException.Type.GRANT_TYPE_EMPTY_ERROR ->
                notifyError(R.string.login_empty_grant_type_error)
            LoginException.Type.CLIENT_ID_EMPTY_ERROR ->
                notifyError(R.string.overall_generic_error)
        }
    }

    private fun notifyError(resId: Int) {
        this.loginLiveData.setNextValue(LoginState.LocalizedError(resId))
    }

    private fun notifyException(message: String) {
        this.loginLiveData.setNextValue(LoginState.Exception(message))
    }
}