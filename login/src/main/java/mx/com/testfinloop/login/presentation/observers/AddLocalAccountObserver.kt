package mx.com.testfinloop.login.presentation.observers

import android.os.Bundle
import android.view.View
import mx.com.testfinloop.core.data.android.lifecycle.SmartLiveDataAndroid
import mx.com.testfinloop.core.domain.exceptions.TestException
import mx.com.testfinloop.core.presentation.observers.DefaultObserver
import mx.com.testfinloop.login.R
import mx.com.testfinloop.login.domain.AddLocalAccountException
import mx.com.testfinloop.login.presentation.states.AddLocalAccountState
import java.io.IOException

class AddLocalAccountObserver(
    private val addLocalAccountLiveData: SmartLiveDataAndroid<AddLocalAccountState>
) : DefaultObserver<Bundle, AddLocalAccountState>() {

    override fun onStart() {
        super.onStart()
        this.addLocalAccountLiveData.value = AddLocalAccountState.LoadingVisibility(View.VISIBLE)
    }

    override fun onComplete() {
        this.addLocalAccountLiveData.value = AddLocalAccountState.LoadingVisibility(View.GONE)
    }

    override fun onNext(bundle: Bundle) {
        this.addLocalAccountLiveData.value = AddLocalAccountState.Data(bundle)
    }

    override fun onError(error: Throwable) {
        this.onComplete()
        when (error) {
            is IOException -> notifyError(R.string.no_internet_connection)
            is AddLocalAccountException -> useCase(error)
            is TestException -> notifyException(error.localizedMessage ?: "")
            else -> notifyError(R.string.overall_generic_error)
        }
    }

    private fun useCase(error: AddLocalAccountException) {
        when (error.validationType) {
            AddLocalAccountException.Type.USERNAME_VALIDATION_ERROR -> notifyError(R.string.login_empty_user_error)
            AddLocalAccountException.Type.PASSWORD_VALIDATION_ERROR -> notifyError(R.string.login_empty_password_error)
            AddLocalAccountException.Type.CREATE_ACCOUNT_VALIDATION_ERROR -> notifyError(R.string.login_adding_account_error)
            AddLocalAccountException.Type.ACCOUNT_ALREADY_EXIST_VALIDATION_ERROR -> notifyError(R.string.only_one_account_per_device)
        }
    }

    private fun notifyError(resId: Int) {
        this.addLocalAccountLiveData.value = AddLocalAccountState.LocalizedError(resId)
    }

    private fun notifyException(message: String) {
        this.addLocalAccountLiveData.value = AddLocalAccountState.Exception(message)
    }
}