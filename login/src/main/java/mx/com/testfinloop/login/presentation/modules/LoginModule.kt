package mx.com.testfinloop.login.presentation.modules

import dagger.Module
import dagger.Provides
import mx.com.testfinloop.core.presentation.scopes.FragmentScope
import mx.com.testfinloop.login.data.repositoryimplementations.LoginRepositoryImpl
import mx.com.testfinloop.login.data.services.LoginService
import mx.com.testfinloop.login.domain.repositoryabstractions.LoginRepository
import mx.com.testfinloop.login.presentation.viewmodels.abstractions.LoginViewModel
import mx.com.testfinloop.login.presentation.viewmodels.implementations.LoginViewModelImpl
import retrofit2.Retrofit
import javax.inject.Named

@Module
class LoginModule {

    @Provides
    @FragmentScope
    fun provideSignInService(@Named("main_retrofit") retrofit: Retrofit): LoginService =
        retrofit.create(LoginService::class.java)

    @Provides
    @FragmentScope
    fun provideSignInRepository(loginRepositoryImpl: LoginRepositoryImpl): LoginRepository =
        loginRepositoryImpl

    @Provides
    @FragmentScope
    fun provideLoginViewModel(loginViewModelImpl: LoginViewModelImpl): LoginViewModel =
        loginViewModelImpl

}