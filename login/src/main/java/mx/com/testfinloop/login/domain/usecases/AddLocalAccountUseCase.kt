package mx.com.testfinloop.login.domain.usecases

import android.accounts.Account
import android.os.Bundle
import io.reactivex.Observable
import mx.com.testfinloop.core.domain.executors.PostExecutionThread
import mx.com.testfinloop.core.domain.executors.ThreadExecutor
import mx.com.testfinloop.core.domain.usecases.UseCase
import mx.com.testfinloop.core.presentation.managers.AccountsManager
import mx.com.testfinloop.login.domain.AddLocalAccountException
import mx.com.testfinloop.login.domain.entities.request.AddLocalAccountRequest
import javax.inject.Inject

class AddLocalAccountUseCase @Inject constructor(
    private val accountsManager: AccountsManager,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<Bundle, AddLocalAccountRequest>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: AddLocalAccountRequest): Observable<Bundle> = Observable.just(params)
        .flatMap {
            when {
                this.accountsManager.getFdaAccounts().isNotEmpty() -> Observable.error(
                    AddLocalAccountException(AddLocalAccountException.Type.ACCOUNT_ALREADY_EXIST_VALIDATION_ERROR)
                )
                else -> {
                    val account = Account(params.signInRequest.email, params.accountType)
                    if (this.accountsManager.addAccountExplicitly(
                            account,
                            params.signInRequest.password,
                            it.loginResponse.data
                        )
                    ) {
                        Observable.just(it.loginResponse.data)
                    } else {
                        Observable.error(AddLocalAccountException(AddLocalAccountException.Type.CREATE_ACCOUNT_VALIDATION_ERROR))
                    }
                }
            }
        }
}