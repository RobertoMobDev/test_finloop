package mx.com.testfinloop.login.presentation.components

import dagger.Component
import mx.com.testfinloop.core.presentation.components.ApplicationComponent
import mx.com.testfinloop.core.presentation.scopes.FragmentScope
import mx.com.testfinloop.login.presentation.fragments.LoginFragment
import mx.com.testfinloop.login.presentation.modules.LoginModule

@FragmentScope
@Component(dependencies = [ApplicationComponent::class], modules = [LoginModule::class])
interface LoginComponent {
    fun inject(loginFragment: LoginFragment)
}