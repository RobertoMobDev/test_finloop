package mx.com.testfinloop.login.presentation.states

import android.os.Bundle

sealed class AddLocalAccountState {
    data class Data(val data: Bundle) : AddLocalAccountState()
    data class LocalizedError(val error: Int) : AddLocalAccountState()
    data class Exception(val error: String) : AddLocalAccountState()
    data class LoadingVisibility(val visibility: Int) : AddLocalAccountState()
}