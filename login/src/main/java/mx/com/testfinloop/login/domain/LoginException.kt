package mx.com.testfinloop.login.domain

class LoginException @JvmOverloads constructor(
    val validationType: Type,
    message: String? = "SignIn ${validationType.name}",
    cause: Throwable? = null
) :
    Throwable(message, cause) {

    enum class Type {
        CLIENT_ID_EMPTY_ERROR,
        USERNAME_EMPTY_ERROR,
        USERNAME_WRONG_FORMAT_ERROR,
        PASSWORD_EMPTY_ERROR,
        GRANT_TYPE_EMPTY_ERROR
    }
}