package mx.com.testfinloop.login.data.datasourceimplementations

import io.reactivex.Observable
import mx.com.testfinloop.core.presentation.managers.ResponseManager
import mx.com.testfinloop.login.data.services.LoginService
import mx.com.testfinloop.login.domain.datasourceabstractions.LoginDataSource
import mx.com.testfinloop.login.domain.entities.request.LoginRequest
import mx.com.testfinloop.login.domain.entities.response.LoginResponse
import javax.inject.Inject

class LoginDataSourceImpl @Inject constructor(private val loginService: LoginService) : LoginDataSource {

    override fun login(loginRequest: LoginRequest): Observable<LoginResponse> =
        this.loginService.login(loginRequest).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(ResponseManager.processErrorResponse(it))
            }
        }
}