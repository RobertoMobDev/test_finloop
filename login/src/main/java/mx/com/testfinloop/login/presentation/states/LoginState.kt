package mx.com.testfinloop.login.presentation.states

import mx.com.testfinloop.login.domain.entities.response.LoginResponse

sealed class LoginState {
    data class Login(val data: LoginResponse) : LoginState()
    data class LocalizedError(val error: Int) : LoginState()
    data class Exception(val error: String) : LoginState()
    data class LoadingVisibility(val visibility: Int) : LoginState()
    data class UserError(val error: Int): LoginState()
    data class PasswordError(val error: Int): LoginState()
}