package mx.com.testfinloop.login.presentation.activities

import android.os.Bundle
import mx.com.testfinloop.core.presentation.activities.BaseActivity
import mx.com.testfinloop.login.R
import mx.com.testfinloop.login.presentation.fragments.LoginFragment

class LoginActivity : BaseActivity() {

    override fun getLayoutResId(): Int = R.layout.activity_login

    override fun initView(savedInstanceState: Bundle?) {
        this.replaceFragmentNoAnimation(LoginFragment(), R.id.frame_login_container, false)
    }
}