package mx.com.testfinloop.login.presentation.viewmodels.abstractions

import mx.com.testfinloop.core.data.android.lifecycle.SmartLiveDataAndroid
import mx.com.testfinloop.login.domain.entities.request.AddLocalAccountRequest
import mx.com.testfinloop.login.domain.entities.request.LoginRequest
import mx.com.testfinloop.login.presentation.states.AddLocalAccountState
import mx.com.testfinloop.login.presentation.states.LoginState

interface LoginViewModel {
    fun login(loginRequest: LoginRequest): SmartLiveDataAndroid<LoginState>
    fun addLocalAccount(addLocalAccountRequest: AddLocalAccountRequest): SmartLiveDataAndroid<AddLocalAccountState>
}