package mx.com.testfinloop.login.presentation.utils

import java.util.regex.Pattern

object SignInDataValidator {

    fun isValidClientId(clientId: String): Boolean = clientId.isNotEmpty()

    fun isValidGrantType(grantType: String): Boolean = grantType.isNotEmpty()

    fun isValidUserName(userName: String): Boolean = userName.isNotEmpty()

    fun isValidPassword(password: String): Boolean = password.isNotEmpty()

    fun isValidUserNameFormat(userName: String): Boolean {
        val emailPattern =
            ("(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
        val pattern = Pattern.compile(emailPattern)
        val matcher = pattern.matcher(userName)
        return matcher.matches()
    }

}