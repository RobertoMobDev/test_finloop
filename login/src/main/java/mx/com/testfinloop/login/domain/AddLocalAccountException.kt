package mx.com.testfinloop.login.domain

class AddLocalAccountException @JvmOverloads constructor(
    val validationType: Type,
    message: String? = "AddLocalAccount ${validationType.name}",
    cause: Throwable? = null
) :
    Throwable(message, cause) {

    enum class Type {
        USERNAME_VALIDATION_ERROR,
        PASSWORD_VALIDATION_ERROR,
        CREATE_ACCOUNT_VALIDATION_ERROR,
        ACCOUNT_ALREADY_EXIST_VALIDATION_ERROR
    }
}