package mx.com.testfinloop.login.presentation.viewmodels.implementations

import mx.com.testfinloop.core.data.android.lifecycle.SmartLiveDataAndroid
import mx.com.testfinloop.core.presentation.viewmodels.BaseViewModelLiveData
import mx.com.testfinloop.login.domain.entities.request.AddLocalAccountRequest
import mx.com.testfinloop.login.domain.entities.request.LoginRequest
import mx.com.testfinloop.login.domain.entities.response.LoginResponse
import mx.com.testfinloop.login.domain.usecases.AddLocalAccountUseCase
import mx.com.testfinloop.login.domain.usecases.LoginUseCase
import mx.com.testfinloop.login.presentation.observers.AddLocalAccountObserver
import mx.com.testfinloop.login.presentation.observers.LoginObserver
import mx.com.testfinloop.login.presentation.states.AddLocalAccountState
import mx.com.testfinloop.login.presentation.states.LoginState
import mx.com.testfinloop.login.presentation.viewmodels.abstractions.LoginViewModel
import javax.inject.Inject

class LoginViewModelImpl @Inject
constructor(
    private val signInUseCase: LoginUseCase,
    private val addLocalAccountUseCase: AddLocalAccountUseCase
) : BaseViewModelLiveData<LoginResponse>(), LoginViewModel {

    private val mLoginLiveData: SmartLiveDataAndroid<LoginState>
        get() = SmartLiveDataAndroid()

    private val mAddLocalAccountLiveData: SmartLiveDataAndroid<AddLocalAccountState>
        get() = SmartLiveDataAndroid()

    override fun login(loginRequest: LoginRequest): SmartLiveDataAndroid<LoginState> =
        with(this.mLoginLiveData) {
            signInUseCase.execute(LoginObserver(this), loginRequest)
            this
        }

    override fun addLocalAccount(addLocalAccountRequest: AddLocalAccountRequest): SmartLiveDataAndroid<AddLocalAccountState> =
        with(this.mAddLocalAccountLiveData) {
            addLocalAccountUseCase.execute(AddLocalAccountObserver(this), addLocalAccountRequest)
            this
        }

    override fun onCleared() {
        super.onCleared()
        this.signInUseCase.dispose()
        this.addLocalAccountUseCase.dispose()
    }
}