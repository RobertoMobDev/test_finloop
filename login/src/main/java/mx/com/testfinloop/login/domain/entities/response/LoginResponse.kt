package mx.com.testfinloop.login.domain.entities.response

import android.os.Bundle
import com.google.gson.annotations.SerializedName
import mx.com.testfinloop.core.domain.entities.AuthToken

data class LoginResponse(@field:SerializedName("id") private val _id: String? = null,
                         @field:SerializedName("jwt") private val _jwt: String? = null) {

    val id: String
        get() = this._id ?: ""

    val jwt: String
        get() = this._jwt ?: ""

    val data: Bundle
        get() = Bundle().apply {
            putString(AuthToken.AUTH_TOKEN, jwt)
        }
}