package mx.com.testfinloop.login.domain.usecases

import io.reactivex.Observable
import mx.com.testfinloop.core.domain.executors.PostExecutionThread
import mx.com.testfinloop.core.domain.executors.ThreadExecutor
import mx.com.testfinloop.core.domain.usecases.UseCase
import mx.com.testfinloop.core.presentation.managers.UserDataManager
import mx.com.testfinloop.login.domain.entities.request.LoginRequest
import mx.com.testfinloop.login.domain.entities.response.LoginResponse
import javax.inject.Inject

class SaveUserUseCase @Inject constructor(
    private val userDataManager: UserDataManager,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<LoginResponse, LoginResponse>(threadExecutor, postExecutionThread) {

    private lateinit var loginRequest: LoginRequest

    override fun createObservable(params: LoginResponse): Observable<LoginResponse> = Observable.just(params)
        .map {
            this.userDataManager.saveUserEmail(it.id)
            this.userDataManager.saveAuthToken(it.jwt)
            it
        }

    fun saveData(request: LoginRequest, response: LoginResponse): Observable<LoginResponse> {
        this.loginRequest = request
        return createObservable(response)
    }
}