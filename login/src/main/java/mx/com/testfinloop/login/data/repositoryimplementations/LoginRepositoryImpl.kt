package mx.com.testfinloop.login.data.repositoryimplementations

import io.reactivex.Observable
import mx.com.testfinloop.login.data.datasourceimplementations.LoginDataSourceImpl
import mx.com.testfinloop.login.domain.entities.request.LoginRequest
import mx.com.testfinloop.login.domain.entities.response.LoginResponse
import mx.com.testfinloop.login.domain.repositoryabstractions.LoginRepository
import javax.inject.Inject

class LoginRepositoryImpl @Inject constructor(private val loginDataSourceImpl: LoginDataSourceImpl) :
    LoginRepository {

    override fun login(loginRequest: LoginRequest): Observable<LoginResponse> =
        this.loginDataSourceImpl.login(loginRequest)
}