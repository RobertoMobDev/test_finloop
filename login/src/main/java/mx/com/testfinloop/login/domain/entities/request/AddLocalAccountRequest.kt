package mx.com.testfinloop.login.domain.entities.request

import mx.com.testfinloop.login.domain.entities.response.LoginResponse

data class AddLocalAccountRequest(
    val signInRequest: LoginRequest,
    val loginResponse: LoginResponse,
    val accountType: String
)