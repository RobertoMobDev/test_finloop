package mx.com.testfinloop.login.data.services

import io.reactivex.Observable
import mx.com.testfinloop.login.domain.entities.request.LoginRequest
import mx.com.testfinloop.login.domain.entities.response.LoginResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService {

    @POST("/login")
    fun login(@Body loginRequest: LoginRequest): Observable<Response<LoginResponse>>

}