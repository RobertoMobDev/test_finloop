package mx.com.testfinloop.login.domain.usecases

import io.reactivex.Observable
import mx.com.testfinloop.core.domain.executors.PostExecutionThread
import mx.com.testfinloop.core.domain.executors.ThreadExecutor
import mx.com.testfinloop.core.domain.usecases.UseCase
import mx.com.testfinloop.login.domain.LoginException
import mx.com.testfinloop.login.domain.entities.request.LoginRequest
import mx.com.testfinloop.login.presentation.utils.SignInDataValidator
import javax.inject.Inject


class ValidateLoginUseCase @Inject constructor(
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<LoginRequest, LoginRequest>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: LoginRequest): Observable<LoginRequest> = when {
        !SignInDataValidator.isValidUserName(params.email) ->
            Observable.error(LoginException(LoginException.Type.USERNAME_EMPTY_ERROR))
        !SignInDataValidator.isValidUserNameFormat(params.email) ->
            Observable.error(LoginException(LoginException.Type.USERNAME_WRONG_FORMAT_ERROR))
        !SignInDataValidator.isValidPassword(params.password) ->
            Observable.error(LoginException(LoginException.Type.PASSWORD_EMPTY_ERROR))
        else -> Observable.just(params)
    }
}