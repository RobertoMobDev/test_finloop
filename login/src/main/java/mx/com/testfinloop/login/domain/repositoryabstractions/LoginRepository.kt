package mx.com.testfinloop.login.domain.repositoryabstractions

import io.reactivex.Observable
import mx.com.testfinloop.login.domain.entities.request.LoginRequest
import mx.com.testfinloop.login.domain.entities.response.LoginResponse

interface LoginRepository {
    fun login(loginRequest: LoginRequest): Observable<LoginResponse>
}