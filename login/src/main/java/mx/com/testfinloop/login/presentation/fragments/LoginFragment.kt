package mx.com.testfinloop.login.presentation.fragments

import android.app.ProgressDialog
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.pawegio.kandroid.textWatcher
import kotlinx.android.synthetic.main.fragment_login.*
import mx.com.testfinloop.core.presentation.fragments.BaseFragment
import mx.com.testfinloop.core.presentation.getApplicationComponent
import mx.com.testfinloop.core.presentation.utils.clickEvent
import mx.com.testfinloop.core.presentation.utils.observeWithValidations
import mx.com.testfinloop.login.R
import mx.com.testfinloop.login.domain.entities.request.AddLocalAccountRequest
import mx.com.testfinloop.login.domain.entities.request.LoginRequest
import mx.com.testfinloop.login.domain.entities.response.LoginResponse
import mx.com.testfinloop.login.presentation.components.DaggerLoginComponent
import mx.com.testfinloop.login.presentation.components.LoginComponent
import mx.com.testfinloop.login.presentation.modules.LoginModule
import mx.com.testfinloop.login.presentation.states.AddLocalAccountState
import mx.com.testfinloop.login.presentation.states.LoginState
import mx.com.testfinloop.login.presentation.viewmodels.abstractions.LoginViewModel
import javax.inject.Inject

class LoginFragment : BaseFragment() {

    @Inject
    lateinit var loginViewModel: LoginViewModel

    private lateinit var dialog: ProgressDialog

    private var loginResponse: LoginResponse = LoginResponse()

    private val loginComponent: LoginComponent by lazy {
        DaggerLoginComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .loginModule(LoginModule())
            .build()
    }

    private val loginRequest: LoginRequest
        get() = LoginRequest(
            this.edit_login_email.text.toString(),
            this.edit_login_password.text.toString()
        )

    private val addLocalAccountRequest: AddLocalAccountRequest
        get() = AddLocalAccountRequest(
            loginRequest,
            loginResponse,
            requireActivity().packageName
        )

    override fun getLayout(): Int = R.layout.fragment_login

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.loginComponent.inject(this)

        this.dialog = ProgressDialog(requireActivity())
        this.dialog.setMessage("Iniciando sesión...")

        this.setPasswordTypeFace()

        this.setClickListeners()

        this.clearErrors()

        this.setEditorListener()

    }

    private fun setEditorListener() {
        this.edit_login_password.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                this.performLogin(this.loginRequest)
            }
            true
        }
    }

    private fun setPasswordTypeFace() {
        this.edit_login_password.transformationMethod = PasswordTransformationMethod()
    }

    private fun performLogin(request: LoginRequest) {
        this.loginViewModel.login(request).observeWithValidations(viewLifecycleOwner) {
            when (it) {
                is LoginState.Login -> {
                    this.loginResponse = it.data
                    this.userDataManager.saveUserEmail(this.edit_login_email.text.toString())
                    this.userDataManager.saveUserPassword(this.edit_login_password.text.toString())
                    addLocalAccount(addLocalAccountRequest)
                }
                is LoginState.LocalizedError -> displayAlert(it.error)
                is LoginState.Exception -> displayAlert(message = it.error)
                is LoginState.LoadingVisibility -> showLoading(it.visibility)
                is LoginState.UserError -> this.text_input_login_email.error = getString(it.error)
                is LoginState.PasswordError ->
                    this.text_input_login_password.error = getString(it.error)
            }
        }
    }

    private fun addLocalAccount(request: AddLocalAccountRequest) {
        this.loginViewModel.addLocalAccount(request).observeWithValidations(viewLifecycleOwner) {
            when (it) {
                is AddLocalAccountState.Data -> {
                    //goToHome()
                    print("sucess")
                }
            }
        }
    }

    private fun showLoading(visibility: Int) {
        if (visibility == View.VISIBLE) {
            dialog.show()
        } else {
            dialog.dismiss()
        }
    }

    private fun setClickListeners() {
        this.button_login.clickEvent().observe(this, Observer {
            this.performLogin(this.loginRequest)
        })
    }

    private fun clearErrors() {
        this.edit_login_email.textWatcher {
            onTextChanged { _, _, _, _ ->
                if (this@LoginFragment.text_input_login_email.isErrorEnabled) {
                    this@LoginFragment.text_input_login_email.isErrorEnabled = false
                    this@LoginFragment.text_input_login_email.error = ""
                }
                checkButtonAvailability()
            }
        }

        this.edit_login_password.textWatcher {
            onTextChanged { _, _, _, _ ->
                if (this@LoginFragment.text_input_login_password.isErrorEnabled) {
                    this@LoginFragment.text_input_login_password.isErrorEnabled = false
                    this@LoginFragment.text_input_login_password.error = ""
                }
                checkButtonAvailability()
            }
        }
    }

    private fun checkButtonAvailability() {
        this.button_login.isEnabled = this.edit_login_email.text.toString().isNotEmpty() &&
                this.edit_login_password.text.toString().isNotEmpty()
    }

    private fun displayAlert(resId: Int? = null, message: String? = null) {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle("Error")
        builder.setMessage(if (resId != null) getString(resId) else message ?: "")

        builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
            dialog.dismiss()
        }
        builder.show()
    }
}